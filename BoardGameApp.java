import java.util.Scanner;

public class BoardGameApp {
	public static void main (String [] args){
		/*Test 1: Create a Tile Variable amd set it to one of the values. Print it using the getter method.
		Tile test1 = Tile.WALL;
		System.out.println("Printing test: "+ test1.getName());
		*/
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to 2D Array Game!");
		Board gameBoard = new Board();
		//Initialize values for numCastles and turns
		int numCastles = 7;
		int turns = 0;
		
		//Main Game Loop
		while(numCastles > 0 && turns < 8){
			System.out.println(gameBoard.toString());
			System.out.println("Number of Castles: " + numCastles);
			System.out.println("Number of turns: " + turns);
			//Ask the user to enter coordinates to place their castle
			System.out.println("Where do you want to place your castle?");
			int row = reader.nextInt();
			int col = reader.nextInt();
			int tokenPlacement = gameBoard.placeToken(row, col);
			//If user enters invalid coordinates, ask them to re-enter valid ones
			while(tokenPlacement < 0){
				System.out.println("Invalid move! Please try again.");
				System.out.println("Where do you want to place your castle?");
				row = reader.nextInt();
				col = reader.nextInt();
				tokenPlacement = gameBoard.placeToken(row, col);
			}
			if(tokenPlacement == 1){
				System.out.println("There is a wall there");
				turns++;
			} else if(tokenPlacement == 0){
				System.out.println("Castle succesfully placed");
				numCastles--;
				turns++;
			}
		}
		//Final Board and Game Results
		System.out.println(gameBoard.toString());
		if(numCastles == 0){
			System.out.println("Congrats! You Won!");
		} else {
			System.out.println("You lost!");
		}
	}
}