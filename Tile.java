public enum Tile {
	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("C");
	
	private final String NAME;
	
	//setter methods
	private Tile(String name){
		this.NAME = name;
	}
	
	//getter method
	public String getName() {
		return this.NAME;
	}
}