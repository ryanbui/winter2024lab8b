import java.util.Random;

public class Board {
	private Tile[][] grid;
	private final int SIZE = 5;
	
	//Constructor for Board
	public Board() {
		Random rng = new Random();
		this.grid = new Tile[this.SIZE][this.SIZE];
		for(int i = 0; i < this.grid.length; i++){
			int randomIndex = rng.nextInt(this.SIZE);
			for(int j = 0; j < this.grid[i].length; j++){
				if(j == randomIndex){
					this.grid[i][j] = Tile.HIDDEN_WALL;
				} else {
					this.grid[i][j] = Tile.BLANK;
				}
			}
		}
	}	
	
	//Method to override toString method
	public String toString(){
		String result = "";
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				result += this.grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	
	//This method, using an int row and col as parameters, will check for the status of tile where the player wishes to place their token 
	public int placeToken(int row, int col){
		if(row >= 0 && row < this.SIZE && col >= 0 && col < this.SIZE){
			if(this.grid[row][col].equals(Tile.BLANK)){
				this.grid[row][col] = Tile.CASTLE;
				return 0;
			} else if(this.grid[row][col].equals(Tile.HIDDEN_WALL)){
			this.grid[row][col] = Tile.WALL;
			return 1;
			} else if(this.grid[row][col].equals(Tile.CASTLE) || this.grid[row][col].equals(Tile.WALL)){
			return -1;
			}	
	}
		return -2;
	}
	


}